﻿using PSU_MoreTutor.Models;
using Microsoft.EntityFrameworkCore;using Microsoft.AspNetCore.Identity.EntityFrameworkCore;namespace PSU_MoreTutor.Data{	public class PSU_MoreTutorContext : IdentityDbContext<NewsUser>
    {
        public DbSet<StdTutor> StdTutor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)        {
            optionsBuilder.UseSqlite(@"Data source=PSU_MoreTutor.db");
        }
 }}