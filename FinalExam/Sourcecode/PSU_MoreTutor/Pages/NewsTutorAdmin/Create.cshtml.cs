using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PSU_MoreTutor.Data;
using PSU_MoreTutor.Models;

namespace PSU_MoreTutor.Pages.NewsTutorAdmin
{
    public class CreateModel : PageModel
    {
        private readonly PSU_MoreTutor.Data.PSU_MoreTutorContext _context;

        public CreateModel(PSU_MoreTutor.Data.PSU_MoreTutorContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public StdTutor StdTutor { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.StdTutor.Add(StdTutor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}