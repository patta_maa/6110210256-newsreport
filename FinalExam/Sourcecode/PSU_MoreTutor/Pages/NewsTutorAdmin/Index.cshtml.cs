using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSU_MoreTutor.Data;
using PSU_MoreTutor.Models;

namespace PSU_MoreTutor.Pages.NewsTutorAdmin
{
    public class IndexModel : PageModel
    {
        private readonly PSU_MoreTutor.Data.PSU_MoreTutorContext _context;

        public IndexModel(PSU_MoreTutor.Data.PSU_MoreTutorContext context)
        {
            _context = context;
        }

        public IList<StdTutor> StdTutor { get;set; }

        public async Task OnGetAsync()
        {
            StdTutor = await _context.StdTutor
                .Include(s => s.postUser).ToListAsync();
        }
    }
}
