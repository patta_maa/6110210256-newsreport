using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PSU_MoreTutor.Data;
using PSU_MoreTutor.Models;

namespace PSU_MoreTutor.Pages.NewsTutorAdmin
{
    public class EditModel : PageModel
    {
        private readonly PSU_MoreTutor.Data.PSU_MoreTutorContext _context;

        public EditModel(PSU_MoreTutor.Data.PSU_MoreTutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public StdTutor StdTutor { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            StdTutor = await _context.StdTutor
                .Include(s => s.postUser).FirstOrDefaultAsync(m => m.StdTutorID == id);

            if (StdTutor == null)
            {
                return NotFound();
            }
           ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(StdTutor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StdTutorExists(StdTutor.StdTutorID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool StdTutorExists(string id)
        {
            return _context.StdTutor.Any(e => e.StdTutorID == id);
        }
    }
}
