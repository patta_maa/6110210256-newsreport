using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSU_MoreTutor.Data;
using PSU_MoreTutor.Models;

namespace PSU_MoreTutor.Pages.NewsTutorAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PSU_MoreTutor.Data.PSU_MoreTutorContext _context;

        public DetailsModel(PSU_MoreTutor.Data.PSU_MoreTutorContext context)
        {
            _context = context;
        }

        public StdTutor StdTutor { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            StdTutor = await _context.StdTutor
                .Include(s => s.postUser).FirstOrDefaultAsync(m => m.StdTutorID == id);

            if (StdTutor == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
