﻿using System;
using System.ComponentModel.DataAnnotations;using Microsoft.AspNetCore.Identity;namespace PSU_MoreTutor.Models{	public class NewsUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}	public class StdTutor {		public string StdTutorID {get; set;}		public string Name { get; set;}		public string Subject { get; set; }		[DataType(DataType.Date)]		public string TutorDate { get; set; }		public string Time { get; set; }		public string Location { get; set; }		public string NumberStd { get; set; }		public int    Rate { get; set; }		public string NewsUserId { get; set; }
		public NewsUser postUser { get; set; }	}}