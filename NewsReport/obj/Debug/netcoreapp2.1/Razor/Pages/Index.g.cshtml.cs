#pragma checksum "D:\Web\NewsReport\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "27a3f676969970c6615e10f6877e80edeaf6a2cd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(NewsReport.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(NewsReport.Pages.Pages_Index), null)]
namespace NewsReport.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Web\NewsReport\Pages\_ViewImports.cshtml"
using NewsReport;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"27a3f676969970c6615e10f6877e80edeaf6a2cd", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88c01d45ccfb24904f468bbbfdaedb0a8afbac7d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "D:\Web\NewsReport\Pages\Index.cshtml"
  
    ViewData["Title"] = "ระบบรายงานข่าว";

#line default
#line hidden
            BeginContext(76, 1053, true);
            WriteLiteral(@"
<script src=""https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/build/ol.js""></script>
<link rel=""stylesheet"" href=""https://unpkg.com/leaflet@1.6.0/dist/leaflet.css""
 integrity=""sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==""crossorigin=""""/>
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src=""https://unpkg.com/leaflet@1.6.0/dist/leaflet.js""
 integrity=""sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==""crossorigin=""""></script>

<div class=""jumbotron"">
    <center><h2>ระบบรายงานข่าวโดยประชาชนเพื่อประชาชน</h2></center>
</div>

<div class=""row"">
    <div id=""mapid"" style=""height: 400px;""></div>
        <script>var mymap = L.map('mapid').setView([6.99, 100.48], 5);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href=""https://www.openstreetmap.org/copyright"">OpenStreetMap</a> contributors'}).addTo(mymap);
");
            WriteLiteral("        </script>\r\n</div>\r\n\r\n");
            EndContext();
#line 26 "D:\Web\NewsReport\Pages\Index.cshtml"
 foreach (var item in Model.NewsCategory){

#line default
#line hidden
            BeginContext(1173, 37, true);
            WriteLiteral("     <div class=\"row\">\r\n         <h3>");
            EndContext();
            BeginContext(1211, 38, false);
#line 28 "D:\Web\NewsReport\Pages\Index.cshtml"
        Write(Html.DisplayFor(model=>item.ShortName));

#line default
#line hidden
            EndContext();
            BeginContext(1249, 19, true);
            WriteLiteral("</h3>\r\n        <h4>");
            EndContext();
            BeginContext(1269, 37, false);
#line 29 "D:\Web\NewsReport\Pages\Index.cshtml"
       Write(Html.DisplayFor(model=>item.FullName));

#line default
#line hidden
            EndContext();
            BeginContext(1306, 286, true);
            WriteLiteral(@"</h4>
    </div>
    <div class=""row"">
        <table class=""table"">
            <thead>
                <tr>
                    <th>วันที่เกิดเหตุ</th><th>เนื้อข่าว</th>
                    <th>สถานะการยืนยัน</th>
                </tr>
            </thead>
        <tbody>
");
            EndContext();
#line 40 "D:\Web\NewsReport\Pages\Index.cshtml"
  foreach (var newsItem in Model.News) {
    if(newsItem.NewsCategoryID == item.NewsCategoryID){
    

#line default
#line hidden
            BeginContext(1697, 48, true);
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1746, 48, false);
#line 45 "D:\Web\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem =>newsItem.ReportDate));

#line default
#line hidden
            EndContext();
            BeginContext(1794, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1850, 48, false);
#line 48 "D:\Web\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem =>newsItem.NewsDetail));

#line default
#line hidden
            EndContext();
            BeginContext(1898, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1954, 48, false);
#line 51 "D:\Web\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem =>newsItem.NewsStatus));

#line default
#line hidden
            EndContext();
            BeginContext(2002, 36, true);
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
            EndContext();
#line 54 "D:\Web\NewsReport\Pages\Index.cshtml"
      }
 }

#line default
#line hidden
            BeginContext(2051, 41, true);
            WriteLiteral("        </tbody>\r\n    </table>\r\n </div>\r\n");
            EndContext();
#line 59 "D:\Web\NewsReport\Pages\Index.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
